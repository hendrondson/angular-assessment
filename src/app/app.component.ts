import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlaceholderService } from './assessment-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  //declare models for this component

  users
  frmID:number 
  choices:Object[] = [{cat: 'people'},{cat: 'planets'},{cat: 'vehicles'},{cat: 'species'}, 
  {cat: 'starships'}]
  whichCategory:string 
  model
  //declare functions for this component

  constructor(private placeholderService:PlaceholderService){

  }

  handleClick(){
    //invoke the service, passing parameters
    this.placeholderService.getParamData(this.frmID,this.whichCategory)
    .subscribe((result)=>{this.model=result})
  }

  // call a method from our service

  invokeService(){
    this.placeholderService.getData().subscribe( (result)=>{
      this.users = result,
      console.log(result)} );
  }


  ngOnInit(){
    //make a call for user data
    this.invokeService()
  }
}