import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlaceholderService {
  
  apiURL:string = 'https://swapi.co/api/'
  constructor(private http:HttpClient) { }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation 
 * @param result 
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    console.error(error); 

    // Let the app keep running by returning an empty result.
    return throwError('Something bad happened; please try again later');
  };
}


  //declare methods of the service

  getData(){
    return this.http.get(`${this.apiURL}`)
    .pipe(
      catchError(this.handleError<Object[]>('getData', []))
    );
}

  getParamData(id, category){
    return this.http.get(`${this.apiURL}${category}/${id}`)
    .pipe(
      catchError(this.handleError<Object[]>('getParamData', []))
    );
  }
}