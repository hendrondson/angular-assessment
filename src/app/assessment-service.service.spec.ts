import { TestBed } from '@angular/core/testing';

import { PlaceholderService } from './assessment-service.service';

describe('AssessmentServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaceholderService = TestBed.get(PlaceholderService);
    expect(service).toBeTruthy();
  });
});
